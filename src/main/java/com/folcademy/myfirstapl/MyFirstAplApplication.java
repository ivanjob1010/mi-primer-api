package com.folcademy.myfirstapl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirstAplApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFirstAplApplication.class, args);
	}

}
