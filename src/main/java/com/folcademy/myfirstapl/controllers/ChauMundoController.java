package com.folcademy.myfirstapl.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChauMundoController {
    @PostMapping("/chau")
    public String ChauMundo(){
        return "Chau Mundo";
    }
}
