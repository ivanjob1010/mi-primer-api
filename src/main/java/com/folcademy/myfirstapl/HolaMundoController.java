package com.folcademy.myfirstapl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaMundoController {
    @RequestMapping("/")
    public String HolaMundo(){
        return "Hola Mundo";
    }
}
